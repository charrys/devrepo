﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace MyFirstGame
{
    public class PlayerCollision : MonoBehaviour
    {
        private int lifes = 4;
        [SerializeField] private Text scoreText;
        [SerializeField] private GameManager gameManager;

        void Start()
        {
            scoreText.text = lifes.ToString();
        }

        void Update()
        {
            scoreText.text = lifes.ToString();
            if(this.lifes < 0)
                gameManager.Endgame();
        }

        void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.tag == "Obstacle")
                this.lifes--;
            else if (collision.collider.tag == "FinishLine")
            {
                if(this.lifes > 0 )
                    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
        }
    }
}
