﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MyFirstGame
{
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] private Rigidbody rb;
        [SerializeField] private GameManager gameManager;
        private float forwardForce = 4000f, sidewaysForce = 50f;
        [SerializeField] private KeyCode keyForw, keyBack, keyRight, keyLeft;

        void FixedUpdate()
        {
            if (Input.GetKey(keyForw))
                rb.AddForce(0, 0, forwardForce * Time.deltaTime);
            else if (Input.GetKey(keyBack))
                rb.AddForce(0, 0, -forwardForce * Time.deltaTime);
            else if (Input.GetKey(keyRight))
                rb.AddForce(sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
            else if (Input.GetKey(keyLeft))
                rb.AddForce(-sidewaysForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);

            if (rb.position.y < -1f)
                gameManager.Endgame();

        }
    }
}
