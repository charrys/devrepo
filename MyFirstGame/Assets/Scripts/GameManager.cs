﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MyFirstGame
{
    public class GameManager : MonoBehaviour
    {
        public void Endgame()
        {
            Debug.Log("GAME OVER");
            Invoke("Restart", 2f);
        }

        public void Restart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
