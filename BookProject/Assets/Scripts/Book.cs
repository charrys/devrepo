﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BookProject
{
    public class Book
    {
        public string Title { get; set; }
        public Dictionary<string, Page> Pages { get; set; }
    }

    public class Page
    {
        public string Text { get; set; }
        public string Title { get; set; }
    }
}
