﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FullSerializer;

namespace BookProject
{
    public class DataController : MonoBehaviour
    {
        [SerializeField] private GameObject Book;
        [SerializeField] private Transform canvas;
        [SerializeField] private KeyCode NextPage, PrevPage;
        [SerializeField] private Text _bookTitle, _pageTitle, _mainText, _pageDetails;

        private List<GameObject> items = new List<GameObject>();
        private static int index = 0;

        void Start()
        {
            Book _book = LoadFromJson();

            for (int i = 0; i < _book.Pages.Count; i++)
            {
                Page value;
                _bookTitle.text = _book.Title;

                if (_book.Pages.TryGetValue(i.ToString(), out value))
                {
                    _pageTitle.text = value.Title;
                    _mainText.text = value.Text;
                    _pageDetails.text = (i + 1).ToString() + "/" + _book.Pages.Count.ToString();
                }

                GameObject gameObject = Instantiate(Book, new Vector3(Book.transform.position.x + i * 800, Book.transform.position.y, 0), Quaternion.identity, canvas);
                items.Add(gameObject);
            }

            Book.SetActive(false);
        }

        void Update()
        {
            if (Input.GetKeyUp(NextPage))
            {
                if (index < items.Count-1)
                {
                    index++;
                    transform.position = Vector3.Lerp(new Vector3(items[index - 1].transform.position.x, items[index - 1].transform.position.y, transform.position.z),
                        new Vector3(items[index].transform.position.x, items[index].transform.position.y, transform.position.z), 1);
                }
            }
            else if (Input.GetKeyUp(PrevPage))
            {
                if (index > 0)
                {
                    index--;
                    transform.position = Vector3.Lerp(new Vector3(items[index+1].transform.position.x, items[index+1].transform.position.y, transform.position.z),
                        new Vector3(items[index].transform.position.x, items[index].transform.position.y, transform.position.z), 1);
                }
            }
        }

        Book LoadFromJson()
        {
            object book = new Book();
            fsSerializer mSerialiser = new fsSerializer();

            var json = Resources.Load<TextAsset>("JSON/book").ToString();
            fsData parsedData = fsJsonParser.Parse(json);

            mSerialiser.TryDeserialize(parsedData, typeof(Book), ref book).AssertSuccess();

            return (Book)book;
        }
    }
}
